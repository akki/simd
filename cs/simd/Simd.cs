﻿namespace LWisteria.Simd
{
	using System.Linq;

	[System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
	struct Vector4
	{
		public double a;
		public double b;
		public double c;
		public double d;

		public static Vector4 Create()
		{
			Vector4 vec = new Vector4();
			return vec;
		}

		public Vector4(double x, double y, double z, double w = 0)
		{
			a = x;
			b = y;
			c = z;
			d = w;
		}

		public double this[int index]
		{
			get
			{
				switch(index)
				{
				case 0:
					return a;
				case 1:
					return b;
				case 2:
					return c;
				case 3:
					return d;
				default:
					throw new System.ArgumentOutOfRangeException();
				}
			}
		}
	}

	interface IVector4Simd
	{
		double this[int i]
		{
			get;
		}
	}

	// 1SIMD=1要素
	struct Vector4Simd1 : IVector4Simd
	{
		public System.Numerics.Vector<double> data0;
		public System.Numerics.Vector<double> data1;
		public System.Numerics.Vector<double> data2;
		public System.Numerics.Vector<double> data3;

		public static Vector4Simd1 Create(Vector4 v)
		{
			var simd = new Vector4Simd1();
			simd.data0 = new System.Numerics.Vector<double>(v.a);
			simd.data1 = new System.Numerics.Vector<double>(v.b);
			simd.data2 = new System.Numerics.Vector<double>(v.c);
			simd.data3 = new System.Numerics.Vector<double>(v.d);
			return simd;
		}

		private static readonly System.Func<Vector4Simd1, double>[] getter =
		{
			/* 0番目の要素 */ (v => v.data0[0]),
			/* 1番目の要素 */ (v => v.data1[0]),
			/* 2番目の要素 */ (v => v.data2[0]),
			/* 3番目の要素 */ (v => v.data3[0]),
		};

		public double this[int i]
		{
			get
			{
				return getter[i](this);
			}
		}
	}
	// 1SIMD=2要素
	struct Vector4Simd2 : IVector4Simd
	{
		public System.Numerics.Vector<double> data0;
		public System.Numerics.Vector<double> data1;

		public static Vector4Simd2 Create(Vector4 v)
		{
			var simd = new Vector4Simd2();
			simd.data0 = new System.Numerics.Vector<double>(new[] { v.a, v.b });
			simd.data1 = new System.Numerics.Vector<double>(new[] { v.c, v.d });
			return simd;
		}

		private static readonly System.Func<Vector4Simd2, double>[] getter =
		{
			/* 0番目の要素 */ (v => v.data0[0]),
			/* 1番目の要素 */ (v => v.data0[1]),
			/* 2番目の要素 */ (v => v.data1[0]),
			/* 3番目の要素 */ (v => v.data1[1]),
		};
		public double this[int i]
		{
			get
			{
				return getter[i](this);
			}
		}
	}
	// 1SIMD=4要素
	struct Vector4Simd4 : IVector4Simd
	{
		public System.Numerics.Vector<double> data0;

		public static Vector4Simd4 Create(Vector4 v)
		{
			var simd = new Vector4Simd4();
			simd.data0 = new System.Numerics.Vector<double>(new[] { v.a, v.b, v.c, v.d });
			return simd;
		}

		public double this[int i]
		{
			get
			{
				return data0[i];
			}
		}
	}

	static class SimdMain
	{
		// なにもしない
		unsafe
		static void Normal(Vector4[] x, Vector4[] v, Vector4[] f, double m, double dt, int n)
		{
			double halfDt2 = dt * dt / 2;
			double rm = 1.0 / m;

			var cb = System.Runtime.InteropServices.Marshal.SizeOf<Vector4>() / sizeof(double);
			fixed (double* X_ = &x[0].a)
			fixed (double* V_ = &v[0].a)
			fixed (double* F_ = &f[0].a)
			{
				var X = X_;
				var V = V_;
				var F = F_;
				for(int i = 0; i < n; i++) {
					for(int j = 0; j < 4; j++) {
						// a = f/m
						var a = F[j] * rm;
						// x += v*dt + a*dt*dt/2
						X[j] += V[j] * dt + a * halfDt2;
						// v += a*dt
						V[j] += a * dt;
					}
					X += cb;
					V += cb;
					F += cb;
				}
			}
		}

		// 1SIMD = 1要素
		static void Simd(Vector4Simd1[] x, Vector4Simd1[] v, Vector4Simd1[] f, double m, double dt_, int n)
		{
			var halfDt2 = new System.Numerics.Vector<double>(dt_ * dt_ / 2);
			var rm = new System.Numerics.Vector<double>(1.0 / m);
			var dt = new System.Numerics.Vector<double>(dt_);

			for (int i = 0; i < n; i++)
			{
				// a = f/m
				var a0 = f[i].data0 * rm;
				var a1 = f[i].data1 * rm;
				var a2 = f[i].data2 * rm;
				var a3 = f[i].data3 * rm;

				// x += v*dt + a*dt*dt/2
				x[i].data0 += v[i].data0 * dt + a0 * halfDt2;
				x[i].data1 += v[i].data1 * dt + a1 * halfDt2;
				x[i].data2 += v[i].data2 * dt + a2 * halfDt2;
				x[i].data3 += v[i].data3 * dt + a3 * halfDt2;

				// v += a*dt
				v[i].data0 += a0 * dt;
				v[i].data1 += a1 * dt;
				v[i].data2 += a2 * dt;
				v[i].data3 += a3 * dt;
			}
		}
		// 1SIMD = 2要素
		static void Simd(Vector4Simd2[] x, Vector4Simd2[] v, Vector4Simd2[] f, double m, double dt_, int n)
		{
			var halfDt2 = new System.Numerics.Vector<double>(dt_ * dt_ / 2);
			var rm = new System.Numerics.Vector<double>(1.0 / m);
			var dt = new System.Numerics.Vector<double>(dt_);

			for (int i = 0; i < n; i++)
			{
				// a = f/m
				var a0 = f[i].data0 * rm;
				var a1 = f[i].data1 * rm;

				// x += v*dt + a*dt*dt/2
				x[i].data0 += v[i].data0 * dt + a0 * halfDt2;
				x[i].data1 += v[i].data1 * dt + a1 * halfDt2;

				// v += a*dt
				v[i].data0 += a0 * dt;
				v[i].data1 += a1 * dt;
			}
		}
		// 1SIMD = 4要素
		static void Simd(Vector4Simd4[] x, Vector4Simd4[] v, Vector4Simd4[] f, double m, double dt_, int n)
		{
			var halfDt2 = new System.Numerics.Vector<double>(dt_ * dt_ / 2);
			var rm = new System.Numerics.Vector<double>(1.0 / m);
			var dt = new System.Numerics.Vector<double>(dt_);

			for (int i = 0; i < n; i++)
			{
				// a = f/m
				var a = f[i].data0 * rm;
				// x += v*dt + a*dt*dt/2
				x[i].data0 += v[i].data0 * dt + a * halfDt2;
				// v += a*dt
				v[i].data0 += a * dt;
			}
		}

		static int Main()
		{
			const int n = 100000;
			const int loop = 1000;

			var rand = new System.Random(713);
			var f = new Vector4[n];
			var v = new Vector4[n];
			var x = new Vector4[n];
			System.Func<double> generator = () => (rand.NextDouble());
			System.Func<Vector4> generator4 = () => (new Vector4(generator(), generator(), generator()));
			f = f.Select(val => generator4()).ToArray();
			v = v.Select(val => generator4()).ToArray();
			x = x.Select(val => generator4()).ToArray();

			var stopwatch = new System.Diagnostics.Stopwatch();

			const double dt = 0.1;
			const double m = 2.5;

			// なにもしない
			Vector4[] vNormal;
			Vector4[] xNormal;
			{
				xNormal = x.ToArray();
				vNormal = v.ToArray();

				System.Console.Write("Normal: ");

				// Warm up.
				for(int i = 0; i < 10; i++)
				{
					Normal(xNormal, vNormal, f, m, dt, n);
				}

				xNormal = x.ToArray();
				vNormal = v.ToArray();

				stopwatch.Restart();

				for (int i = 0; i < loop; i++)
				{
					Normal(xNormal, vNormal, f, m, dt, n);
				}
				var time = stopwatch.ElapsedMilliseconds;
				System.Console.WriteLine("{0} [ms]", time);
			}

			// SIMD
			var vSimd = new IVector4Simd[n];
			var xSimd = new IVector4Simd[n];
            switch (System.Numerics.Vector<double>.Count)
			{
				case 1:
					{
						{
							var xSimd1 = x.Select(val => Vector4Simd1.Create(val)).ToArray();
							var vSimd1 = v.Select(val => Vector4Simd1.Create(val)).ToArray();

							var fSimd = f.Select(val => Vector4Simd1.Create(val)).ToArray();

							System.Console.Write("Simd({0}bit SIMD): ", 64 * System.Numerics.Vector<double>.Count);

							// Warm up.
							for(int i = 0; i < 10; i++)
							{
								Simd(xSimd1, vSimd1, fSimd, m, dt, n);
							}

							xSimd1 = x.Select(val => Vector4Simd1.Create(val)).ToArray();
							vSimd1 = v.Select(val => Vector4Simd1.Create(val)).ToArray();
							fSimd = f.Select(val => Vector4Simd1.Create(val)).ToArray();

							stopwatch.Restart();

							for (int i = 0; i < loop; i++)
							{
								Simd(xSimd1, vSimd1, fSimd, m, dt, n);
							}

							var time = stopwatch.ElapsedMilliseconds;
							System.Console.WriteLine("{0} [ms]", time);

							vSimd1.CopyTo(vSimd, 0);
							xSimd1.CopyTo(xSimd, 0);
						}
					}
					break;
				case 2:
					{
						{
							var xSimd2 = x.Select(val => Vector4Simd2.Create(val)).ToArray();
							var vSimd2 = v.Select(val => Vector4Simd2.Create(val)).ToArray();

							var fSimd = f.Select(val => Vector4Simd2.Create(val)).ToArray();

							System.Console.Write("Simd({0}bit SIMD): ", 64 * System.Numerics.Vector<double>.Count);

							// Warm up.
							for(int i = 0; i < 10; i++)
							{
								Simd(xSimd2, vSimd2, fSimd, m, dt, n);
							}

							xSimd2 = x.Select(val => Vector4Simd2.Create(val)).ToArray();
							vSimd2 = v.Select(val => Vector4Simd2.Create(val)).ToArray();
							fSimd = f.Select(val => Vector4Simd2.Create(val)).ToArray();

							stopwatch.Restart();

							for (int i = 0; i < loop; i++)
							{
								Simd(xSimd2, vSimd2, fSimd, m, dt, n);
							}

							var time = stopwatch.ElapsedMilliseconds;
							System.Console.WriteLine("{0} [ms]", time);

							vSimd2.CopyTo(vSimd, 0);
							xSimd2.CopyTo(xSimd, 0);
						}
					}
					break;
				case 4:
					{
						{
							var xSimd4 = x.Select(val => Vector4Simd4.Create(val)).ToArray();
							var vSimd4 = v.Select(val => Vector4Simd4.Create(val)).ToArray();

							var fSimd = f.Select(val => Vector4Simd4.Create(val)).ToArray();

							System.Console.Write("Simd({0}bit SIMD): ", 64 * System.Numerics.Vector<double>.Count);

							// Warm up.
							for(int i = 0; i < 10; i++)
							{
								Simd(xSimd4, vSimd4, fSimd, m, dt, n);
							}

							xSimd4 = x.Select(val => Vector4Simd4.Create(val)).ToArray();
							vSimd4 = v.Select(val => Vector4Simd4.Create(val)).ToArray();
							fSimd = f.Select(val => Vector4Simd4.Create(val)).ToArray();

							stopwatch.Restart();

							for (int i = 0; i < loop; i++)
							{
								Simd(xSimd4, vSimd4, fSimd, m, dt, n);
							}

							var time = stopwatch.ElapsedMilliseconds;
							System.Console.WriteLine("{0} [ms]", time);

							vSimd4.CopyTo(vSimd, 0);
							xSimd4.CopyTo(xSimd, 0);
						}
					}
					break;
				default:
					throw new System.ApplicationException("サポートされていないSIMD長");
			}


			// エラーチェック
			const double eps = 1e-8;
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					double errorX = System.Math.Abs((xNormal[i][j] - xSimd[i][j]) / xNormal[i][j]);
					if (errorX > eps)
					{
						System.Console.WriteLine("errorX[{0}][{1}] = {2}", i, j, errorX);
					}

					double errorV = System.Math.Abs((vNormal[i][j] - vSimd[i][j]) / vNormal[i][j]);
					if (errorV > eps)
					{
						System.Console.WriteLine("errorV[{0}][{1}] = {2}", i, j, errorV);
					}
				}
			}

			return System.Environment.ExitCode;
		}
	}
}